package othlon.stopgap.items;


/**
 * Created by Othlon Nova on 4/05/2014.
 */

import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import othlon.stopgap.items.items.gapItems;


@Mod(modid = References.MODID, name = References.MODNAME, version = References.VERSION)
public class StopGap {


    @Mod.EventHandler
    public static void preInit(FMLPreInitializationEvent event)
    {
       gapItems.init();
       gapItems.initOreDictionary();

    }//preinit


    @Mod.EventHandler
    public void init (FMLInitializationEvent event)
    {

    }//fml init


}//StopGap
