package othlon.stopgap.items.items;

/**
 * Created by JayRay on 11/05/2014.
 */
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import othlon.stopgap.items.References;

public class othlonItems extends Item
{

        public String getUnwrappedUnlocalizedName(String unlocalizedName)
        {
            return unlocalizedName.substring(unlocalizedName.indexOf(".") + 1);
        }

        @Override
        public String getUnlocalizedName()
        {
            return String.format("item.%s%s", References.RESOURCESPREFIX, getUnwrappedUnlocalizedName(super.getUnlocalizedName()));
        }

        @Override
        public String getUnlocalizedName(ItemStack itemStack)
        {
            return String.format("item.%s%s", References.RESOURCESPREFIX, getUnwrappedUnlocalizedName(super.getUnlocalizedName()));
        }

        @Override
        @SideOnly(Side.CLIENT)
        public void registerIcons(IIconRegister iconRegister)
        {
            this.itemIcon = iconRegister.registerIcon(References.RESOURCESPREFIX + getUnwrappedUnlocalizedName(super.getUnlocalizedName()));
        }

}
