package othlon.stopgap.items.items;

/**
 * Created by JayRay on 7/05/2014.
 */

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.item.Item;
import net.minecraftforge.oredict.OreDictionary;

public class gapItems {

    public static Item ingotRed;
    public static Item mixRed;
    public static Item ingotGlow;
    public static Item mixGlow;


    public static void init()
    {

        /* redstone stuff */
        ingotRed = new itmIngotRed();
        GameRegistry.registerItem(ingotRed, "ingotRed");

        mixRed   = new itmMixRed();
        GameRegistry.registerItem(mixRed, "mixRed");


        /* Glowstone stuff*/
        ingotGlow = new itmIngotGlow();
        GameRegistry.registerItem(ingotGlow, "ingotGlow");

        mixGlow   = new itmMixGlow();
        GameRegistry.registerItem(mixGlow, "mixGlow");

    }//init items

    public static void initOreDictionary()
    {
        OreDictionary.registerOre("ingotRedstone", ingotRed);

        OreDictionary.registerOre("ingotGlowstone", ingotGlow);


    }//init ore dict


}
