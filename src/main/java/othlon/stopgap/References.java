package othlon.stopgap.items;

/**
 * Created by JayRay on 11/05/2014.
 */
public class References {

    public static final String MODID = "StopGap";
    public static final String MODNAME = "StopGap";
    public static final String VERSION = "1.6.0";



    public static final String RESOURCESPREFIX = MODID.toLowerCase() + ":";

}
